INSERT INTO major VALUES('1','คอมพิวเตอร์ธุรกิจ','Computer Business');
INSERT INTO major VALUES('2','วิทยาการคอมพิวเตอร์','Computer Science');
INSERT INTO major VALUES('3','เศษฐศาสตร์','Economic');

INSERT INTO title VALUES('1','นาย','Mr.');
INSERT INTO title VALUES('2','นางสาว','Miss');
INSERT INTO title VALUES('3','นาง','Mrs.');

INSERT INTO student VALUES('57161111','ดุ๊กดิ๊ก','ใจดี','Dukdik','Jaidee','2','1');
INSERT INTO student VALUES('57162222','ด๊อกแด๊ก','ใจเย็น','Toktak','Jaiyen','2','2');
INSERT INTO student VALUES('57163333','บูรพา','สุขใจ','Burapha','Sukjai','1','3');
