CREATE VIEW SalePerOrder AS
	SELECT
	orderNumber, SUM(quantityOrdered * priceEach) total
FROM
	orderdetails
GROUP BY orderNumber
ORDER BY total DESC;
