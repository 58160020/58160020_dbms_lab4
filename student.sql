DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS title;
DROP TABLE IF EXISTS major;
CREATE TABLE title
(
	tid 		VARCHAR(5),
	tname 		VARCHAR(30),
	ename		VARCHAR(30),
	PRIMARY KEY(tid)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE major
(
	id		VARCHAR(5),
	tname		VARCHAR(30),
	ename		VARCHAR(30),
	PRIMARY KEY(id)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE student
(
	id 		VARCHAR(10),
	tname		VARCHAR(30),
	tsurname	VARCHAR(50),
	ename		VARCHAR(30),
	esurname	VARCHAR(50),
	title_id	VARCHAR(5),
	major_id	VARCHAR(5),
	PRIMARY KEY (id),
	FOREIGN KEY (title_id) REFERENCES title(tid),
	FOREIGN KEY (major_id) REFERENCES major(id)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
